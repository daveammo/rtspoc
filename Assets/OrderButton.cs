﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrderButton : MonoBehaviour {

    private Text orderName;
    private Button button;
    // Use this for initialization
	void Start () {
		
	}

    void Awake()
    {
        orderName = GetComponentInChildren<Text>();
        button = GetComponentInChildren<Button>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetButtonName(string name)
    {
        orderName.text = name;
    }

    internal void SetImage(Sprite action)
    {
        button.GetComponent<Image>().sprite = action;
    }
}
