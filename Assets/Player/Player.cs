﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTS;

public class Player : MonoBehaviour
{
    public string username;
    public bool human;
    public HUD hud;
    public WorldObject SelectedObject { get; set; }

    public int startMoney, startMoneyLimit, startPower, startPowerLimit;
    private Dictionary<Enums.ResourceType, int> resources, resourceLimits;


    // Use this for initialization
    void Start()
    {
        hud = GetComponentInChildren<HUD>();
        AddStartResourceLimits();
        AddStartResources();
    }

    // Update is called once per frame
    void Update()
    {
        if (human)
        {
            hud.SetResourceValues(resources, resourceLimits);
        }
    }

    void Awake()
    {
        resources = InitResourceList();
        resourceLimits = InitResourceList();
    }

    private Dictionary<Enums.ResourceType, int> InitResourceList()
    {
        Dictionary<Enums.ResourceType, int> list = new Dictionary<Enums.ResourceType, int>();
        list.Add(Enums.ResourceType.Money, 0);
        list.Add(Enums.ResourceType.Power, 0);
        return list;
    }

    private void AddStartResourceLimits()
    {
        IncrementResourceLimit(Enums.ResourceType.Money, startMoneyLimit);
        IncrementResourceLimit(Enums.ResourceType.Power, startPowerLimit);
    }

    private void AddStartResources()
    {
        AddResource(Enums.ResourceType.Money, startMoney);
        AddResource(Enums.ResourceType.Power, startPower);
    }

    public void AddResource(Enums.ResourceType type, int amount)
    {
        resources[type] += amount;
    }

    public void IncrementResourceLimit(Enums.ResourceType type, int amount)
    {
        resourceLimits[type] += amount;
    }

    public void AddUnit(string unitName, Vector3 spawnPoint, Quaternion rotation)
    {
        Debug.Log("add " + unitName + " to player");
    }

}

