﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTS;
using UnityEngine.UI;


public class HUD : MonoBehaviour {
    private RectTransform[] bars;
    private Player player;
    private const int SELECTION_NAME_HEIGHT = 15;
    public Text selectedNameLabel;
    public GUISkin selectBoxSkin, mouseCursorSkin;
    private const int ICON_WIDTH = 32, ICON_HEIGHT = 32, TEXT_WIDTH = 128, TEXT_HEIGHT = 32;                
    public Resource[] resources;
    private WorldObject lastSelection;
    private float sliderValue;
    public Texture2D buttonHover, buttonClick;
    private int buildAreaHeight = 0;
    private const int BUTTON_SPACING = 7;
    private const int SCROLL_BAR_WIDTH = 22;
    public RectTransform ordersBar, resourceBar;
    public RectTransform orderDrawSpace;
    public OrderButton OrderButton;

    public Texture2D activeCursor;
    public Texture2D selectCursor, leftCursor, rightCursor, upCursor, downCursor;
    public Texture2D[] moveCursors, attackCursors, harvestCursors;
    private Enums.CursorState activeCursorState;
    private int currentFrame = 0;
    private const int BUILD_IMAGE_WIDTH = 64, BUILD_IMAGE_HEIGHT = 64;

    private Dictionary<Enums.ResourceType, int> resourceValues, resourceLimits;

    // Use this for initialization
    void Start () {
        resourceValues = new Dictionary<Enums.ResourceType, int>();
        resourceLimits = new Dictionary<Enums.ResourceType, int>();        
        for (int i = 0; i < resources.Length; i++)
        {
            switch (resources[i].type)
            {
                case Enums.ResourceType.Money:                    
                    resourceValues.Add(Enums.ResourceType.Money, 0);
                    resourceLimits.Add(Enums.ResourceType.Money, 0);
                    break;
                case Enums.ResourceType.Power:                    
                    resourceValues.Add(Enums.ResourceType.Power, 0);
                    resourceLimits.Add(Enums.ResourceType.Power, 0);
                    break;
                default: break;
            }
        }
        buildAreaHeight = Screen.height - (int)ResourceManager.ResourceBarHEight - SELECTION_NAME_HEIGHT - 2 * BUTTON_SPACING;
        player = transform.root.GetComponent<Player>();
        bars = transform.GetComponents<RectTransform>();
        foreach (RectTransform rt in bars)
        {
            switch (rt.gameObject.name)
            {
                case "OrdersBar":
                    rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, ResourceManager.OrdersBarWidth);
                    break;
                case "ResourceBar":
                    rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, ResourceManager.ResourceBarHEight);
                    break;
            }
        }
        ResourceManager.StoreSelectBoxItems(selectBoxSkin);
        SetCursorState(Enums.CursorState.Select);
    }
    void Awake()
    {
        
    }
	// Update is called once per frame
	void OnGUI () {
        if (player.human)
        {
            //TODO: Do we need to hide this for performance on NPCs
            DrawOrdersBar();
            DrawResourcesBar();
            DrawMouseCursor();
        }
        
    }

    public bool MouseInBounds()
    {
        //Screen coordinates start in the lower-left corner of the screen
        //not the top-left of the screen like the drawing coordinates do
        Vector3 mousePos = Input.mousePosition;
        bool insideWidth = mousePos.x >= 0 && mousePos.x <= Screen.width - ResourceManager.OrdersBarWidth;
        bool insideHeight = mousePos.y >= 0 && mousePos.y <= Screen.height - ResourceManager.ResourceBarHEight;
        return insideWidth && insideHeight;
    }
    private void DrawResourcesBar()
    {
        foreach (Resource r in resources)
        {
            string labelVal = String.Format("{0}/{1}", resourceValues[r.type].ToString(), resourceLimits[r.type].ToString());
            r.label.text = labelVal;
        }

    }
    
    private void DrawOrdersBar()
    {
        string selectionName = "";
        if (player.SelectedObject)
        {
            selectionName = player.SelectedObject.objectName;
        }
        if (!selectionName.Equals(""))
        {
            selectedNameLabel.text = selectionName;
        }
        if (player.SelectedObject.IsOwnedBy(player))
        {
            //Draw new orders if the selection has changed
            if (lastSelection && lastSelection != player.SelectedObject)
            {                
                DrawActions(player.SelectedObject.GetActions());
            }
            //store the current selection
            lastSelection = player.SelectedObject;
        }
    }
    public Rect GetPlayingArea()
    {
        return new Rect(0, ResourceManager.ResourceBarHEight, Screen.width - ResourceManager.OrdersBarWidth, Screen.height - ResourceManager.ResourceBarHEight);
    }

    private void DrawMouseCursor()
    {
        bool mouseOverHud = !MouseInBounds() && activeCursorState != Enums.CursorState.PanRight && activeCursorState != Enums.CursorState.PanUp;
        if (mouseOverHud)
        {
            Cursor.visible = true;
        }
        else {
            Cursor.visible = false;
            GUI.skin = mouseCursorSkin;
            GUI.BeginGroup(new Rect(0, 0, Screen.width, Screen.height));
            UpdateCursorAnimation();
            Rect cursorPosition = GetCursorDrawPosition();
            GUI.Label(cursorPosition, activeCursor);
            GUI.EndGroup();
        }
    }
    private void UpdateCursorAnimation()
    {
        //sequence animation for cursor (based on more than one image for the cursor)
        //change once per second, loops through array of images
        if (activeCursorState == Enums.CursorState.Move)
        {
            currentFrame = (int)(Time.time * ResourceManager.CursorBlinkRate % moveCursors.Length);
            activeCursor = moveCursors[currentFrame];
        }
        else if (activeCursorState == Enums.CursorState.Attack)
        {
            currentFrame = (int)(Time.time * ResourceManager.CursorBlinkRate % attackCursors.Length);
            activeCursor = attackCursors[currentFrame];
        }
        else if (activeCursorState == Enums.CursorState.Harvest)
        {
            currentFrame = (int)(Time.time * ResourceManager.CursorBlinkRate % harvestCursors.Length);
            activeCursor = harvestCursors[currentFrame];
        }
    }
    private Rect GetCursorDrawPosition()
    {
        //set base position for custom cursor image
        float leftPos = Input.mousePosition.x;
        float topPos = Screen.height - Input.mousePosition.y; //screen draw coordinates are inverted
                                                              //adjust position base on the type of cursor being shown
        if (activeCursorState == Enums.CursorState.PanRight) leftPos = Screen.width - ResourceManager.OrdersBarWidth - activeCursor.width;
        else if (activeCursorState == Enums.CursorState.PanDown) topPos = Screen.height - activeCursor.height;
        else if (activeCursorState == Enums.CursorState.Move || activeCursorState == Enums.CursorState.Select || activeCursorState == Enums.CursorState.Harvest)
        {
            topPos -= activeCursor.height / 2;
            leftPos -= activeCursor.width / 2;
        }
        return new Rect(leftPos, topPos, activeCursor.width, activeCursor.height);
    }
    public void SetCursorState(Enums.CursorState newState)
    {
        activeCursorState = newState;
        switch (newState)
        {
            case Enums.CursorState.Select:
                activeCursor = selectCursor;
                break;
            case Enums.CursorState.Attack:
                currentFrame = (int)Time.time % attackCursors.Length;
                activeCursor = attackCursors[currentFrame];
                break;
            case Enums.CursorState.Harvest:
                currentFrame = (int)Time.time % harvestCursors.Length;
                activeCursor = harvestCursors[currentFrame];
                break;
            case Enums.CursorState.Move:
                currentFrame = (int)Time.time % moveCursors.Length;
                activeCursor = moveCursors[currentFrame];
                break;
            case Enums.CursorState.PanLeft:
                activeCursor = leftCursor;
                break;
            case Enums.CursorState.PanRight:
                activeCursor = rightCursor;
                break;
            case Enums.CursorState.PanUp:
                activeCursor = upCursor;
                break;
            case Enums.CursorState.PanDown:
                activeCursor = downCursor;
                break;
            default: break;
        }
    }
    public void SetResourceValues(Dictionary<Enums.ResourceType, int> resourceValues, Dictionary<Enums.ResourceType, int> resourceLimits)
    {
        this.resourceValues = resourceValues;
        this.resourceLimits = resourceLimits;
    }

    private void DrawActions(string[] actions)
    {
        foreach (Transform t in orderDrawSpace.transform)
        {
            //TODO: Object Pooling maybe
            Destroy(t.gameObject);
        }
        foreach (string s in actions)
        {
            OrderButton b = (OrderButton)Instantiate(OrderButton, orderDrawSpace);
            b.SetButtonName(s);
            Sprite action = ResourceManager.GetBuildImage(s);
            b.SetImage(action);
        }
    }
    private int MaxNumRows(int areaHeight)
    {
        return areaHeight / BUILD_IMAGE_HEIGHT;
    }

    private Rect GetButtonPos(int row, int column)
    {
        int left = SCROLL_BAR_WIDTH + column * BUILD_IMAGE_WIDTH;
        float top = row * BUILD_IMAGE_HEIGHT - sliderValue * BUILD_IMAGE_HEIGHT;
        return new Rect(left, top, BUILD_IMAGE_WIDTH, BUILD_IMAGE_HEIGHT);
    }

    private void DrawSlider(int groupHeight, float numRows)
    {
        //slider goes from 0 to the number of rows that do not fit on screen
        sliderValue = GUI.VerticalSlider(GetScrollPos(groupHeight), sliderValue, 0.0f, numRows - MaxNumRows(groupHeight));
    }
    private Rect GetScrollPos(int groupHeight)
    {
        return new Rect(BUTTON_SPACING, BUTTON_SPACING, SCROLL_BAR_WIDTH, groupHeight - 2 * BUTTON_SPACING);
    }
}