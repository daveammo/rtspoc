﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS {
    public class Enums
    {
        public enum CursorState { Select, Move, Attack, PanLeft, PanRight, PanUp, PanDown, Harvest }
        public enum ResourceType { Money, Power}
    }
}
